package avion;

import jdk.jshell.spi.ExecutionControl;

import java.util.Arrays;
import java.util.Scanner;

public class EjercicioAvion {

    public static Scanner teclado = new Scanner(System.in);

    public static final int NUMERO_SILLAS = 10;
    public static final String TEXTO_MENU = "Seleccione una opcion: \n" +
            "1. Ingresar Pasajero\n" +
            "2. Peso Total Avion\n" +
            "3. Peso Promedio Pasajero\n" +
            "4. Pasajero Gordito\n" +
            "5. Contenido Avion\n" +
            "6. Sillas disponibles\n" +
            "7. Salir";

    private static double[] pesos = new double[NUMERO_SILLAS];

    private static String[] nombres = new String[NUMERO_SILLAS];

    private static int numeroPasajeros = 0;

    private static double pesoTotal = 0;

    private static double pesoGordito = 0;
    private static int poscicionGordito = 0;


    public static void imprimir_avion() {
        System.out.println("Mi avion tiene");
        System.out.println(Arrays.toString(pesos));
        System.out.println(Arrays.toString(nombres));
    }

    /**
     * Permite asignar un pasajero a la silla
     *
     * @param nombre El nombre del pasajero
     * @param peso   El peso del pasajero
     * @param silla  La poscicion de la silla a solicitar
     * @return "Ok" Si la silla estaba disponible o "No disponible" Cuando esta ocupada
     */
    public static String ingresarAvion(String nombre, double peso, int silla) {
        if (nombres[silla] == null) {
            nombres[silla] = nombre;
            pesos[silla] = peso;
            numeroPasajeros ++;
            pesoTotal += peso;
            if(pesoGordito < peso) {
                pesoGordito = peso;
                poscicionGordito = silla;
            }
            return "Ok";
        } else {
            return "No disponible";
        }
    }

    public static double calcularPesoTotal() {
        /*//crear una variable del calcularPesoTotal
        double pesoTotal = 0;
        //for (int i = 0; i < pesos.length; i++) {//para cada peso en pesos
        for (double peso: pesos) {
            //Acumular el peso
            pesoTotal += peso;
        }
        //retornar el calcularPesoTotal*/
        return pesoTotal;
    }

    public static double calcularPesoPromedio(){
        return numeroPasajeros != 0 ? pesoTotal / numeroPasajeros : 0;
    }

    public static String elGordito(){
        return nombres[poscicionGordito] != null ? nombres[poscicionGordito] : "No hay pasajeros en el avión";
    }

    public static int calcularSillasDisponibles() {
        return NUMERO_SILLAS - numeroPasajeros;
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Bienvenido a reservas de Avión");
        int seleccion = 0;

        while (seleccion !=7){
            System.out.println(TEXTO_MENU);
            seleccion = teclado.nextInt();
            switch (seleccion){
                case 1:
                    System.out.println("Ingrese el nombre del pasajero");
                    String nombre = teclado.next();
                    System.out.println("Ingrese el peso del pasajero");
                    double peso = teclado.nextDouble();
                    System.out.println("Ingrese la silla");
                    int silla = teclado.nextInt();
                    String resultado = ingresarAvion(nombre,peso, silla);
                    if(resultado == "Ok"){
                        System.out.println("Se ingresó el Pasajero");
                    }else{
                        System.err.println(resultado);
                    }
                    break;
                case 2:
                    System.out.println("El peso total es " + calcularPesoTotal());
                    break;
                case 3:
                    System.out.println("El peso promedio es " + calcularPesoPromedio());
                    break;
                case 4:
                    System.out.println("El pasajero gordito es" + elGordito());
                    break;
                case 5:
                    imprimir_avion();
                    break;
                case 6:
                    System.out.println(calcularSillasDisponibles());
                    break;
                case 7:
                    break;
                default:
                    System.out.println("Seleccion Invalida");
                    break;
            }
        }
        System.out.println("Hasta luego");
    }

}
