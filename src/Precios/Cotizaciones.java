package Precios;

import java.util.Arrays;
//15
public class Cotizaciones {
    public static final int FILAS_PRODUCTOS =3 ;
    public static final int COLUMNAS_PROVEEDORES = 4 ;
    private static double precios [] [] = new double [FILAS_PRODUCTOS][COLUMNAS_PROVEEDORES];
    private static final String[] PRODUCTOS = {"|     Ipad     |", "|   Iphone 6   |", "|Samsung Galaxy|"};
    private static final String[] PROVEEDORES = {"A", "B", "C", "D"};



    /**
     * Inicializa con los datos de productos y precios
     */
    public static void inicializarBaseDeDatos () {
        double[] fila = {285, 300, 290, 284.99 };
        precios[0] = fila;
        fila = new double[]{559, 560, 565, 560};
        precios[1] = fila;
        fila = new double[]{82, 85, 80, 79};
        precios[2] = fila;
    }

    public static String aString(){
        String s = "";
        s += "|  Productos   |";
        for (String proveedor :
             PROVEEDORES) {
            s += "   " + proveedor + "   |";
        }
        s += "\n";
        for (int fila = 0; fila < precios.length; fila++) {
            s += PRODUCTOS[fila];
            for (int columna = 0; columna < precios[fila].length ; columna++) {
                s += " " + precios[fila][columna] + " |";
            }
            s += "\n";
        }
        return s;
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        inicializarBaseDeDatos();
        System.out.println(aString());
    }
}
