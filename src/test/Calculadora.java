package test;

import jdk.nashorn.api.tree.ForInLoopTree;

public class Calculadora {

    public static int sumar(int a , int b){
        return a + b;
    }

    public static int resta(int x, int y){
        return x - y;
    }

    public static int producto(int x, int y){
        return x * y;
    }

    public static int division(int x, int y){
        return x / y;
    }

    public static int modulo(int x, int y){
        return x % y;
    }


}
