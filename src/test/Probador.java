package test;
public class Probador {

    public static void probarSuma(){
        System.out.println("Probando la funcion suma...");
        int a = 10;
        int b = 9;
        int resultadoEsperado = 19;
        int resultadoReal = Calculadora.sumar(a, b);
        if (resultadoEsperado == resultadoReal){
            System.out.println("La Función sumar está OK");
        }else {
            System.out.println("La Funcion sumar está mal, el resultado real " + resultadoReal
                    + " Es diferente del resultado esperado " + resultadoEsperado);
        }
    }

    public static void probarResta(){
        System.out.println("Probando la funcion resta...");
        int datoPrueba = 2, otroDatoPrueba = 1, resultadoEsperado = 1;
        int resultadoReal = Calculadora.resta(datoPrueba, otroDatoPrueba);
        if (resultadoEsperado == resultadoReal){
            System.out.println("La Función restar está OK");
        }else {
            System.out.println("La Funcion restar está mal, el resultado real " + resultadoReal
                    + " Es diferente del resultado esperado " + resultadoEsperado);
        }
    }

    public static void probarModulo(){
        System.out.println("Probando la funcion Modulo...");
        int datoPrueba = 7, otroDatoPrueba = 3, resultadoEsperado = 1;
        int resultadoReal = Calculadora.modulo(datoPrueba, otroDatoPrueba);
        if (resultadoEsperado == resultadoReal){
            System.out.println("La Función modulo está OK");
        }else {
            System.out.println("La Funcion Modulo está mal, el resultado real " + resultadoReal
                    + " Es diferente del resultado esperado " + resultadoEsperado);
        }
    }

    public static void probarProducto(){
        System.out.println("Probando la funcion Producto...");
        int datoPrueba = 8, otroDatoPrueba = 4, resultadoEsperado = 32;
        int resultadoReal = Calculadora.producto(datoPrueba, otroDatoPrueba);
        if (resultadoEsperado == resultadoReal){
            System.out.println("La Función producto está OK");
        }else {
            System.out.println("La Funcion producto está mal, el resultado real " + resultadoReal
                    + " Es diferente del resultado esperado " + resultadoEsperado);
        }
    }

    public static void probarDivision(){
        System.out.println("Probando la funcion Division...");
        //System.out.println("Probando la función Modulo...");
        int datoPrueba = 8, otroDatoPrueba = 4, resultadoEsperado = 2;
        int resultadoReal = Calculadora.division(datoPrueba, otroDatoPrueba);
        if (resultadoEsperado == resultadoReal){
            System.out.println("La Función Division está OK");
        }else {
            System.out.println("La Funcion Division está mal, el resultado real " + resultadoReal
                    + " Es diferente del resultado esperado " + resultadoEsperado);
        }
    }


    public static void main(String[] args) {
        // Probamos la funcion Suma
        probarSuma();

        // Probamos la resta
        probarResta();

        // Probamos el Modulo
        probarModulo();

        // Probamos el producto
        probarProducto();

        // Probamos la División
        probarDivision();

    }

}
