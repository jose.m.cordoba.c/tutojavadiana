package Matrices;

import java.util.Random;

public class EjerciciosMatrices {

	/**
	 * llena una matriz de tamaño dado por el usuario con muneros hasta el max
	 * 
	 * @param filas    este contiene el numero de filas a generar
	 * @param columnas este contiene el numero de columnas a generar
	 * @param max      es es el limite de los numeros que contiene la matriz
	 * @return retorna el resultado de la matriz
	 */
	public static int[][] generarMatriz(int filas, int columnas, int max) {
		Random generador = new Random();
		// Declarar la matriz con el tamaño esperado

		int[][] matrizResultante = new int[filas][columnas];

		// Recorrer las filas de la matriz

		for (int i = 0; i < matrizResultante.length; i++) {
			// Recorrer las columnas

			for (int n = 0; n < matrizResultante[i].length; n++) {

				matrizResultante[i][n] = generador.nextInt(max);

			} // cierra segundo for

			// Generar un número al azar

			// Asignarlo en la poscicion deseada

		} // cierra primer for

		// Retonar la matriz Generada

		return matrizResultante;
	} // cierra la funcion generarMatriz

	/**
	 * la funcion matrizACadena recorre las filas y las colomnas y las muestra 
	 * @param matriz es una matriz de numeros 
	 * @return es una reoresentacion en formato de texto de la matriz 
	 */
	public static String matrizACadena(int[][] matriz) {

		String resultado = "  ";

		for (int i = 0; i < matriz.length; i++) {

			for (int n = 0; n < matriz[i].length; n++) {

				resultado += matriz[i][n] + "  ,  ";

			}

			resultado += " \n ";

		}
		return resultado;
	}

}// cierra clase