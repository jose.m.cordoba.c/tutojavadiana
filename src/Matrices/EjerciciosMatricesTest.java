package Matrices;

import jdk.jshell.spi.ExecutionControl;

import static Matrices.EjerciciosMatrices.generarMatriz;
import static org.junit.jupiter.api.Assertions.*;

class EjerciciosMatricesTest {

    @org.junit.jupiter.api.Test
    void generarMatrizTest() throws ExecutionControl.NotImplementedException {
        int filas = 4, columnas = 5, max = 10;
        int[][] esperado = new int[filas][columnas];
        int[][] real = generarMatriz(filas, columnas, max);
        assertEquals(esperado.length, real.length);
        assertEquals(esperado[0].length, real[0].length);
    }
}