package EjercciosFunciones;

import jdk.jshell.spi.ExecutionControl.NotImplementedException;

import java.util.Scanner;
import java.lang.Math;

public class Funciones {

    public static Scanner entrada = new Scanner(System.in);

    public static double areaRectangulo(double ladoA, double ladoB) throws NotImplementedException{
        double resultado = ladoA * ladoB;
        return resultado;
    }

    public static double areaCirculo(double radio) throws NotImplementedException{
        //TODO Implementar esto
        throw new NotImplementedException("Implementar esto");
    }

    /**
     * Realiza una función llamada relacion(a, b) que a partir de dos números cumpla lo siguiente:
     *
     *     Si el primer número es mayor que el segundo, debe devolver 1.
     *     Si el primer número es menor que el segundo, debe devolver -1.
     *     Si ambos números son iguales, debe devolver un 0.
     *
     * Comprueba la relación entre los números: '5 y 10', '10 y 5' y '5 y 5'.
     * @return
     */
    public static double relacionAB() throws NotImplementedException {
        throw new NotImplementedException("Paila");

    }


    /**
     * Realiza una función llamada intermedio(a, b) que a partir de dos números,
     * devuelva su punto intermedio. Cuando lo tengas comprueba el punto intermedio entre -12 y 24:
     * @return
     */
    public static double intermedio() throws NotImplementedException {
        throw new NotImplementedException("Paila");
    }

    /**
     * Realiza una función llamada recortar(numero, minimo, maximo) que reciba tres parámetros.
     * El primero es el número a recortar, el segundo es el límite inferior y el tercero el
     * límite superior. La función tendrá que cumplir lo siguiente:
     *
     *     Devolver el límite inferior si el número es menor que éste
     *     Devolver el límite superior si el número es mayor que éste.
     *     Devolver el número sin cambios si no se supera ningún límite.
     *
     * Comprueba el resultado de recortar 15 entre los límites 0 y 10.
     * @return
     */
    public static double recortar() throws NotImplementedException{
        throw new NotImplementedException("Paila");
    }


    /**
     * Realiza una función separar(lista) que tome una lista de números enteros y devuelva dos listas ordenadas.
     * La primera con los números pares y la segunda con los números impares.
     * @return
     */
    public static int[][] separar(int[] numeros) throws NotImplementedException {
        throw new NotImplementedException("Paila");
    }


    public static void main(String[] args) throws NotImplementedException, InterruptedException {
        double lado1 = 3, lado2 = 5, areaEsperada = 15;
        System.out.println("Probando area Rectangulo...");
        Thread.sleep(1000);
        double areaReal = areaRectangulo(lado1, lado2);
        if(areaReal == areaEsperada){
            System.out.println("Al pelo");
        }else{
            System.out.println("No pasó la prueba");
        }

        double radio = 1;
        areaEsperada = Math.PI;
        System.out.println("Probando area circulo...");
        Thread.sleep(1000);
        areaReal = areaCirculo(radio);
        if(areaReal == areaEsperada){
            System.out.println("Al pelo");
        }else{
            System.out.println("No pasó la prueba");
        }

        /*System.out.println("Ingrese un lado");
        lado1 = entrada.nextDouble();

        System.out.println("Ingrese otro lado");
        lado2 = entrada.nextDouble();

        System.out.println("El area de su rectangulo es " +
                areaRectangulo(lado1, lado2));*/

    }
}
